package ru.t1.shipilov.tm.api;

import ru.t1.shipilov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
