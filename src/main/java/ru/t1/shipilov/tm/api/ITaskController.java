package ru.t1.shipilov.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
