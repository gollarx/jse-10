package ru.t1.shipilov.tm.api;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
