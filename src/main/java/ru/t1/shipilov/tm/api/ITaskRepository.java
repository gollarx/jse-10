package ru.t1.shipilov.tm.api;

import ru.t1.shipilov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

}
