package ru.t1.shipilov.tm.api;

import ru.t1.shipilov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
