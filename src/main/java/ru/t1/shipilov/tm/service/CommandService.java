package ru.t1.shipilov.tm.service;

import ru.t1.shipilov.tm.api.ICommandRepository;
import ru.t1.shipilov.tm.api.ICommandService;
import ru.t1.shipilov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
